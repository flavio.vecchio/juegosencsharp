﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Controlador;

namespace Presentacion
{
    public partial class JuegoMasJugado : Form
    {
        private InformeController controller;
        public JuegoMasJugado()
        {
            InitializeComponent();
            controller = new InformeController();
        }

        private void salirBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void JuegoMasJugado_Load(object sender, EventArgs e)
        {
            int cantidadDeJugadasTaTeTi = controller.cantidadJuegos("TaTeTi");
            int cantidadDeJugadasPPT = controller.cantidadJuegos("PiedraPapelTijera");

            juegosJugadosChart.Palette = ChartColorPalette.EarthTones;
            juegosJugadosChart.Titles.Add("Total de partidas por juego");
            juegosJugadosChart.Series.Clear();
            string[] seriesArray = { "TaTeTi", "Piedra-Papel-Tijera" };
            Series series = juegosJugadosChart.Series.Add("Total");
            series.Points.AddXY(seriesArray[0], cantidadDeJugadasTaTeTi);
            series.Points.AddXY(seriesArray[1], cantidadDeJugadasPPT);

            if (cantidadDeJugadasTaTeTi > cantidadDeJugadasPPT) resultadoLbl.Text = "El juego mas jugado es el TaTeTi";
            if (cantidadDeJugadasTaTeTi < cantidadDeJugadasPPT) resultadoLbl.Text = "El juego mas jugado es el Piedra Papel Tijera";
            if (cantidadDeJugadasTaTeTi == cantidadDeJugadasPPT) resultadoLbl.Text = "Ambos juegos tienen la misma cantidad de juegos";

            xmlPathLbl.Text = controller.pathXML();

        }
    }
}
