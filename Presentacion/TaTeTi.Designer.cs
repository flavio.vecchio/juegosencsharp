﻿namespace Presentacion
{
    partial class TaTeTi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.terminarJuegoBtn = new System.Windows.Forms.Button();
            this.turnoActualTb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.puntosPcTb = new System.Windows.Forms.TextBox();
            this.puntosJugadorTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.salirBtn = new System.Windows.Forms.Button();
            this.nuevoJuegoBtn = new System.Windows.Forms.Button();
            this.jugadorCmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.warGamesMode = new System.Windows.Forms.PictureBox();
            this.celda33 = new System.Windows.Forms.PictureBox();
            this.celda32 = new System.Windows.Forms.PictureBox();
            this.celda31 = new System.Windows.Forms.PictureBox();
            this.celda23 = new System.Windows.Forms.PictureBox();
            this.celda22 = new System.Windows.Forms.PictureBox();
            this.celda21 = new System.Windows.Forms.PictureBox();
            this.celda13 = new System.Windows.Forms.PictureBox();
            this.celda12 = new System.Windows.Forms.PictureBox();
            this.celda11 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.warGamesMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda11)).BeginInit();
            this.SuspendLayout();
            // 
            // terminarJuegoBtn
            // 
            this.terminarJuegoBtn.Location = new System.Drawing.Point(675, 74);
            this.terminarJuegoBtn.Name = "terminarJuegoBtn";
            this.terminarJuegoBtn.Size = new System.Drawing.Size(96, 23);
            this.terminarJuegoBtn.TabIndex = 33;
            this.terminarJuegoBtn.Text = "Terminar Juego";
            this.terminarJuegoBtn.UseVisualStyleBackColor = true;
            this.terminarJuegoBtn.Click += new System.EventHandler(this.terminarJuegoBtn_Click);
            // 
            // turnoActualTb
            // 
            this.turnoActualTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.turnoActualTb.Location = new System.Drawing.Point(641, 131);
            this.turnoActualTb.Name = "turnoActualTb";
            this.turnoActualTb.ReadOnly = true;
            this.turnoActualTb.Size = new System.Drawing.Size(135, 30);
            this.turnoActualTb.TabIndex = 32;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(647, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 25);
            this.label5.TabIndex = 31;
            this.label5.Text = "Turno Actual";
            // 
            // puntosPcTb
            // 
            this.puntosPcTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.puntosPcTb.Location = new System.Drawing.Point(574, 410);
            this.puntosPcTb.Name = "puntosPcTb";
            this.puntosPcTb.ReadOnly = true;
            this.puntosPcTb.Size = new System.Drawing.Size(161, 30);
            this.puntosPcTb.TabIndex = 29;
            // 
            // puntosJugadorTb
            // 
            this.puntosJugadorTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.puntosJugadorTb.Location = new System.Drawing.Point(34, 410);
            this.puntosJugadorTb.Name = "puntosJugadorTb";
            this.puntosJugadorTb.ReadOnly = true;
            this.puntosJugadorTb.Size = new System.Drawing.Size(161, 30);
            this.puntosJugadorTb.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(602, 371);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 25);
            this.label3.TabIndex = 26;
            this.label3.Text = "Puntos PC";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 371);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 25);
            this.label2.TabIndex = 25;
            this.label2.Text = "Puntos Jugador 1";
            // 
            // salirBtn
            // 
            this.salirBtn.Location = new System.Drawing.Point(697, 16);
            this.salirBtn.Name = "salirBtn";
            this.salirBtn.Size = new System.Drawing.Size(75, 23);
            this.salirBtn.TabIndex = 24;
            this.salirBtn.Text = "Salir";
            this.salirBtn.UseVisualStyleBackColor = true;
            this.salirBtn.Click += new System.EventHandler(this.salirBtn_Click);
            // 
            // nuevoJuegoBtn
            // 
            this.nuevoJuegoBtn.Location = new System.Drawing.Point(676, 45);
            this.nuevoJuegoBtn.Name = "nuevoJuegoBtn";
            this.nuevoJuegoBtn.Size = new System.Drawing.Size(96, 23);
            this.nuevoJuegoBtn.TabIndex = 23;
            this.nuevoJuegoBtn.Text = "Comenzar Juego";
            this.nuevoJuegoBtn.UseVisualStyleBackColor = true;
            this.nuevoJuegoBtn.Click += new System.EventHandler(this.nuevoJuegoBtn_Click);
            // 
            // jugadorCmb
            // 
            this.jugadorCmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.jugadorCmb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jugadorCmb.FormattingEnabled = true;
            this.jugadorCmb.Location = new System.Drawing.Point(119, 16);
            this.jugadorCmb.Name = "jugadorCmb";
            this.jugadorCmb.Size = new System.Drawing.Size(516, 33);
            this.jugadorCmb.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 25);
            this.label1.TabIndex = 21;
            this.label1.Text = "Jugador 1";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(189, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(329, 10);
            this.label4.TabIndex = 43;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(189, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(329, 10);
            this.label6.TabIndex = 44;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(406, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 301);
            this.label7.TabIndex = 45;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(296, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 301);
            this.label8.TabIndex = 46;
            // 
            // warGamesMode
            // 
            this.warGamesMode.Image = global::Presentacion.Properties.Resources.ModoWarGames;
            this.warGamesMode.Location = new System.Drawing.Point(574, 195);
            this.warGamesMode.Name = "warGamesMode";
            this.warGamesMode.Size = new System.Drawing.Size(198, 173);
            this.warGamesMode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.warGamesMode.TabIndex = 47;
            this.warGamesMode.TabStop = false;
            this.warGamesMode.Click += new System.EventHandler(this.warGamesMode_Click);
            // 
            // celda33
            // 
            this.celda33.Location = new System.Drawing.Point(420, 287);
            this.celda33.Name = "celda33";
            this.celda33.Size = new System.Drawing.Size(88, 88);
            this.celda33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.celda33.TabIndex = 42;
            this.celda33.TabStop = false;
            this.celda33.Click += new System.EventHandler(this.celda33_Click);
            // 
            // celda32
            // 
            this.celda32.Location = new System.Drawing.Point(312, 287);
            this.celda32.Name = "celda32";
            this.celda32.Size = new System.Drawing.Size(88, 88);
            this.celda32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.celda32.TabIndex = 41;
            this.celda32.TabStop = false;
            this.celda32.Click += new System.EventHandler(this.celda32_Click);
            // 
            // celda31
            // 
            this.celda31.Location = new System.Drawing.Point(199, 287);
            this.celda31.Name = "celda31";
            this.celda31.Size = new System.Drawing.Size(88, 88);
            this.celda31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.celda31.TabIndex = 40;
            this.celda31.TabStop = false;
            this.celda31.Click += new System.EventHandler(this.celda31_Click);
            // 
            // celda23
            // 
            this.celda23.Location = new System.Drawing.Point(420, 180);
            this.celda23.Name = "celda23";
            this.celda23.Size = new System.Drawing.Size(88, 88);
            this.celda23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.celda23.TabIndex = 39;
            this.celda23.TabStop = false;
            this.celda23.Click += new System.EventHandler(this.celda23_Click);
            // 
            // celda22
            // 
            this.celda22.Location = new System.Drawing.Point(312, 182);
            this.celda22.Name = "celda22";
            this.celda22.Size = new System.Drawing.Size(88, 88);
            this.celda22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.celda22.TabIndex = 38;
            this.celda22.TabStop = false;
            this.celda22.Click += new System.EventHandler(this.celda22_Click);
            // 
            // celda21
            // 
            this.celda21.Location = new System.Drawing.Point(199, 182);
            this.celda21.Name = "celda21";
            this.celda21.Size = new System.Drawing.Size(88, 88);
            this.celda21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.celda21.TabIndex = 37;
            this.celda21.TabStop = false;
            this.celda21.Click += new System.EventHandler(this.celda21_Click);
            // 
            // celda13
            // 
            this.celda13.Location = new System.Drawing.Point(420, 74);
            this.celda13.Name = "celda13";
            this.celda13.Size = new System.Drawing.Size(88, 88);
            this.celda13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.celda13.TabIndex = 36;
            this.celda13.TabStop = false;
            this.celda13.Click += new System.EventHandler(this.celda13_Click);
            // 
            // celda12
            // 
            this.celda12.Location = new System.Drawing.Point(312, 74);
            this.celda12.Name = "celda12";
            this.celda12.Size = new System.Drawing.Size(88, 88);
            this.celda12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.celda12.TabIndex = 35;
            this.celda12.TabStop = false;
            this.celda12.Click += new System.EventHandler(this.celda12_Click);
            // 
            // celda11
            // 
            this.celda11.Location = new System.Drawing.Point(199, 74);
            this.celda11.Name = "celda11";
            this.celda11.Size = new System.Drawing.Size(88, 88);
            this.celda11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.celda11.TabIndex = 34;
            this.celda11.TabStop = false;
            this.celda11.Click += new System.EventHandler(this.celda11_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(589, 167);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(172, 25);
            this.label9.TabIndex = 48;
            this.label9.Text = "Modo War Games";
            // 
            // TaTeTi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.warGamesMode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.celda33);
            this.Controls.Add(this.celda32);
            this.Controls.Add(this.celda31);
            this.Controls.Add(this.celda23);
            this.Controls.Add(this.celda22);
            this.Controls.Add(this.celda21);
            this.Controls.Add(this.celda13);
            this.Controls.Add(this.celda12);
            this.Controls.Add(this.celda11);
            this.Controls.Add(this.terminarJuegoBtn);
            this.Controls.Add(this.turnoActualTb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.puntosPcTb);
            this.Controls.Add(this.puntosJugadorTb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.salirBtn);
            this.Controls.Add(this.nuevoJuegoBtn);
            this.Controls.Add(this.jugadorCmb);
            this.Controls.Add(this.label1);
            this.Name = "TaTeTi";
            this.Text = "TaTeTi";
            ((System.ComponentModel.ISupportInitialize)(this.warGamesMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celda11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button terminarJuegoBtn;
        private System.Windows.Forms.TextBox turnoActualTb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox puntosPcTb;
        private System.Windows.Forms.TextBox puntosJugadorTb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button salirBtn;
        private System.Windows.Forms.Button nuevoJuegoBtn;
        private System.Windows.Forms.ComboBox jugadorCmb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox celda11;
        private System.Windows.Forms.PictureBox celda12;
        private System.Windows.Forms.PictureBox celda13;
        private System.Windows.Forms.PictureBox celda23;
        private System.Windows.Forms.PictureBox celda22;
        private System.Windows.Forms.PictureBox celda21;
        private System.Windows.Forms.PictureBox celda33;
        private System.Windows.Forms.PictureBox celda32;
        private System.Windows.Forms.PictureBox celda31;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox warGamesMode;
        private System.Windows.Forms.Label label9;
    }
}