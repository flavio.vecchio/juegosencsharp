﻿namespace Presentacion
{
    partial class PiedraPapelTijera
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.jugadorCmb = new System.Windows.Forms.ComboBox();
            this.nuevoJuegoBtn = new System.Windows.Forms.Button();
            this.salirBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.puntosJugadorTb = new System.Windows.Forms.TextBox();
            this.puntosPcTb = new System.Windows.Forms.TextBox();
            this.ganadorRondaTb = new System.Windows.Forms.TextBox();
            this.tijeraImg = new System.Windows.Forms.PictureBox();
            this.piedraImg = new System.Windows.Forms.PictureBox();
            this.papelImg = new System.Windows.Forms.PictureBox();
            this.turnoActualTb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.terminarJuegoBtn = new System.Windows.Forms.Button();
            this.seleccionPcTb = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tijeraImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.piedraImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.papelImg)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Jugador 1";
            // 
            // jugadorCmb
            // 
            this.jugadorCmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.jugadorCmb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jugadorCmb.FormattingEnabled = true;
            this.jugadorCmb.Location = new System.Drawing.Point(116, 17);
            this.jugadorCmb.Name = "jugadorCmb";
            this.jugadorCmb.Size = new System.Drawing.Size(516, 33);
            this.jugadorCmb.TabIndex = 4;
            // 
            // nuevoJuegoBtn
            // 
            this.nuevoJuegoBtn.Location = new System.Drawing.Point(673, 46);
            this.nuevoJuegoBtn.Name = "nuevoJuegoBtn";
            this.nuevoJuegoBtn.Size = new System.Drawing.Size(96, 23);
            this.nuevoJuegoBtn.TabIndex = 5;
            this.nuevoJuegoBtn.Text = "Comenzar Juego";
            this.nuevoJuegoBtn.UseVisualStyleBackColor = true;
            this.nuevoJuegoBtn.Click += new System.EventHandler(this.nuevoJuegoBtn_Click);
            // 
            // salirBtn
            // 
            this.salirBtn.Location = new System.Drawing.Point(694, 17);
            this.salirBtn.Name = "salirBtn";
            this.salirBtn.Size = new System.Drawing.Size(75, 23);
            this.salirBtn.TabIndex = 6;
            this.salirBtn.Text = "Salir";
            this.salirBtn.UseVisualStyleBackColor = true;
            this.salirBtn.Click += new System.EventHandler(this.salirBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 289);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Puntos Jugador 1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(599, 289);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "Puntos PC";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(288, 289);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "Ganador ultimo punto";
            // 
            // puntosJugadorTb
            // 
            this.puntosJugadorTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.puntosJugadorTb.Location = new System.Drawing.Point(31, 328);
            this.puntosJugadorTb.Name = "puntosJugadorTb";
            this.puntosJugadorTb.ReadOnly = true;
            this.puntosJugadorTb.Size = new System.Drawing.Size(161, 30);
            this.puntosJugadorTb.TabIndex = 10;
            // 
            // puntosPcTb
            // 
            this.puntosPcTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.puntosPcTb.Location = new System.Drawing.Point(571, 328);
            this.puntosPcTb.Name = "puntosPcTb";
            this.puntosPcTb.ReadOnly = true;
            this.puntosPcTb.Size = new System.Drawing.Size(161, 30);
            this.puntosPcTb.TabIndex = 11;
            // 
            // ganadorRondaTb
            // 
            this.ganadorRondaTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ganadorRondaTb.Location = new System.Drawing.Point(293, 328);
            this.ganadorRondaTb.Name = "ganadorRondaTb";
            this.ganadorRondaTb.ReadOnly = true;
            this.ganadorRondaTb.Size = new System.Drawing.Size(193, 30);
            this.ganadorRondaTb.TabIndex = 12;
            // 
            // tijeraImg
            // 
            this.tijeraImg.Image = global::Presentacion.Properties.Resources.tijera;
            this.tijeraImg.Location = new System.Drawing.Point(505, 66);
            this.tijeraImg.Name = "tijeraImg";
            this.tijeraImg.Size = new System.Drawing.Size(127, 207);
            this.tijeraImg.TabIndex = 2;
            this.tijeraImg.TabStop = false;
            this.tijeraImg.Click += new System.EventHandler(this.tijeraImg_Click);
            // 
            // piedraImg
            // 
            this.piedraImg.Image = global::Presentacion.Properties.Resources.piedra;
            this.piedraImg.Location = new System.Drawing.Point(116, 66);
            this.piedraImg.Name = "piedraImg";
            this.piedraImg.Size = new System.Drawing.Size(127, 207);
            this.piedraImg.TabIndex = 1;
            this.piedraImg.TabStop = false;
            this.piedraImg.Click += new System.EventHandler(this.piedraImg_Click);
            // 
            // papelImg
            // 
            this.papelImg.Image = global::Presentacion.Properties.Resources.papel;
            this.papelImg.Location = new System.Drawing.Point(323, 66);
            this.papelImg.Name = "papelImg";
            this.papelImg.Size = new System.Drawing.Size(127, 207);
            this.papelImg.TabIndex = 0;
            this.papelImg.TabStop = false;
            this.papelImg.Click += new System.EventHandler(this.papelImg_Click);
            // 
            // turnoActualTb
            // 
            this.turnoActualTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.turnoActualTb.Location = new System.Drawing.Point(638, 148);
            this.turnoActualTb.Name = "turnoActualTb";
            this.turnoActualTb.ReadOnly = true;
            this.turnoActualTb.Size = new System.Drawing.Size(135, 30);
            this.turnoActualTb.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(644, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 25);
            this.label5.TabIndex = 13;
            this.label5.Text = "Turno Actual";
            // 
            // terminarJuegoBtn
            // 
            this.terminarJuegoBtn.Location = new System.Drawing.Point(672, 75);
            this.terminarJuegoBtn.Name = "terminarJuegoBtn";
            this.terminarJuegoBtn.Size = new System.Drawing.Size(96, 23);
            this.terminarJuegoBtn.TabIndex = 15;
            this.terminarJuegoBtn.Text = "Terminar Juego";
            this.terminarJuegoBtn.UseVisualStyleBackColor = true;
            this.terminarJuegoBtn.Click += new System.EventHandler(this.terminarJuegoBtn_Click);
            // 
            // seleccionPcTb
            // 
            this.seleccionPcTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seleccionPcTb.Location = new System.Drawing.Point(638, 243);
            this.seleccionPcTb.Name = "seleccionPcTb";
            this.seleccionPcTb.ReadOnly = true;
            this.seleccionPcTb.Size = new System.Drawing.Size(135, 30);
            this.seleccionPcTb.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(637, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 25);
            this.label6.TabIndex = 16;
            this.label6.Text = "PC Selecciono";
            // 
            // PiedraPapelTijera
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 375);
            this.Controls.Add(this.seleccionPcTb);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.terminarJuegoBtn);
            this.Controls.Add(this.turnoActualTb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ganadorRondaTb);
            this.Controls.Add(this.puntosPcTb);
            this.Controls.Add(this.puntosJugadorTb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.salirBtn);
            this.Controls.Add(this.nuevoJuegoBtn);
            this.Controls.Add(this.jugadorCmb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tijeraImg);
            this.Controls.Add(this.piedraImg);
            this.Controls.Add(this.papelImg);
            this.Name = "PiedraPapelTijera";
            this.Text = "Juego: Piedra Papel o Tijera";
            this.Load += new System.EventHandler(this.PiedraPapelTijera_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tijeraImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.piedraImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.papelImg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox papelImg;
        private System.Windows.Forms.PictureBox piedraImg;
        private System.Windows.Forms.PictureBox tijeraImg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox jugadorCmb;
        private System.Windows.Forms.Button nuevoJuegoBtn;
        private System.Windows.Forms.Button salirBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox puntosJugadorTb;
        private System.Windows.Forms.TextBox puntosPcTb;
        private System.Windows.Forms.TextBox ganadorRondaTb;
        private System.Windows.Forms.TextBox turnoActualTb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button terminarJuegoBtn;
        private System.Windows.Forms.TextBox seleccionPcTb;
        private System.Windows.Forms.Label label6;
    }
}