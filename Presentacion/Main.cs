﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Jugadores p = new Jugadores();
            p.MdiParent = this;
            p.Show();
        }

        private void crearPresupuestoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PiedraPapelTijera p = new PiedraPapelTijera();
            p.MdiParent = this;
            p.Show();
        }

        private void editarPresupuestoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TaTeTi p = new TaTeTi();
            p.MdiParent = this;
            p.Show();
        }

        private void totalPresupuestadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            JuegoMasJugado p = new JuegoMasJugado();
            p.MdiParent = this;
            p.Show();
        }

        private void totalPresupuestadoPorClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HistorialJuegos p = new HistorialJuegos();
            p.MdiParent = this;
            p.Show();

        }

        private void Main_Load(object sender, EventArgs e)
        {

        }
    }
}
