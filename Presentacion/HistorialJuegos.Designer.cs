﻿namespace Presentacion
{
    partial class HistorialJuegos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.jugadorCmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.salirBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.historialTaTeTiChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.historialPiedraPapelTijeraChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.historialTaTeTiChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.historialPiedraPapelTijeraChart)).BeginInit();
            this.SuspendLayout();
            // 
            // jugadorCmb
            // 
            this.jugadorCmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.jugadorCmb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jugadorCmb.FormattingEnabled = true;
            this.jugadorCmb.Location = new System.Drawing.Point(131, 18);
            this.jugadorCmb.Name = "jugadorCmb";
            this.jugadorCmb.Size = new System.Drawing.Size(516, 33);
            this.jugadorCmb.TabIndex = 23;
            this.jugadorCmb.SelectedIndexChanged += new System.EventHandler(this.jugadorCmb_SelectedIndexChanged);
            this.jugadorCmb.Click += new System.EventHandler(this.jugadorCmb_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 25);
            this.label1.TabIndex = 24;
            this.label1.Text = "Jugador";
            // 
            // salirBtn
            // 
            this.salirBtn.Location = new System.Drawing.Point(688, 23);
            this.salirBtn.Name = "salirBtn";
            this.salirBtn.Size = new System.Drawing.Size(75, 23);
            this.salirBtn.TabIndex = 25;
            this.salirBtn.Text = "Salir";
            this.salirBtn.UseVisualStyleBackColor = true;
            this.salirBtn.Click += new System.EventHandler(this.salirBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(79, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 25);
            this.label2.TabIndex = 26;
            this.label2.Text = "TaTeTi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(579, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 25);
            this.label3.TabIndex = 27;
            this.label3.Text = "Piedra Papel Tijera";
            // 
            // historialTaTeTiChart
            // 
            chartArea1.Name = "ChartArea1";
            this.historialTaTeTiChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.historialTaTeTiChart.Legends.Add(legend1);
            this.historialTaTeTiChart.Location = new System.Drawing.Point(17, 137);
            this.historialTaTeTiChart.Name = "historialTaTeTiChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.historialTaTeTiChart.Series.Add(series1);
            this.historialTaTeTiChart.Size = new System.Drawing.Size(312, 300);
            this.historialTaTeTiChart.TabIndex = 28;
            this.historialTaTeTiChart.Text = "chart1";
            // 
            // historialPiedraPapelTijeraChart
            // 
            chartArea2.Name = "ChartArea1";
            this.historialPiedraPapelTijeraChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.historialPiedraPapelTijeraChart.Legends.Add(legend2);
            this.historialPiedraPapelTijeraChart.Location = new System.Drawing.Point(444, 138);
            this.historialPiedraPapelTijeraChart.Name = "historialPiedraPapelTijeraChart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.historialPiedraPapelTijeraChart.Series.Add(series2);
            this.historialPiedraPapelTijeraChart.Size = new System.Drawing.Size(312, 300);
            this.historialPiedraPapelTijeraChart.TabIndex = 29;
            this.historialPiedraPapelTijeraChart.Text = "chart1";
            // 
            // HistorialJuegos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.historialPiedraPapelTijeraChart);
            this.Controls.Add(this.historialTaTeTiChart);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.salirBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.jugadorCmb);
            this.Name = "HistorialJuegos";
            this.Text = "Historial Juegos";
            this.Load += new System.EventHandler(this.HistorialJuegos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.historialTaTeTiChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.historialPiedraPapelTijeraChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox jugadorCmb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button salirBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataVisualization.Charting.Chart historialTaTeTiChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart historialPiedraPapelTijeraChart;
    }
}