﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controlador;

namespace Presentacion
{
    public partial class Jugadores : Form
    {
        private JugadorController controller;

        public Jugadores()
        {
            InitializeComponent();
            controller = new JugadorController(jugadoresDv, idTb, nombreTb, apellidoTb, dniTb, fechaNacimientoDp, emailTb, localidadTb);

        }

        private void salirBn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Jugadores_Load(object sender, EventArgs e)
        {
            controller.inicializar();
            controller.refrescarGrilla();
        }

        private void clearSelection()
        {
            jugadoresDv.ClearSelection();

        }

        private void nuevoBn_Click(object sender, EventArgs e)
        {
            try
            {
                controller.nuevo();
                controller.refrescarGrilla();
                clearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void guardarBn_Click(object sender, EventArgs e)
        {
            try
            {
                controller.grabar();
                controller.refrescarGrilla();
                clearSelection();
                controller.inicializar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void jugadoresDv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                controller.seleccionar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void borrarBn_Click(object sender, EventArgs e)
        {
            try
            {
                controller.borrar();
                controller.refrescarGrilla();
                clearSelection();
                controller.inicializar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
