﻿namespace Presentacion
{
    partial class JuegoMasJugado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.salirBtn = new System.Windows.Forms.Button();
            this.juegosJugadosChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.resultadoLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.xmlPathLbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.juegosJugadosChart)).BeginInit();
            this.SuspendLayout();
            // 
            // salirBtn
            // 
            this.salirBtn.Location = new System.Drawing.Point(683, 29);
            this.salirBtn.Name = "salirBtn";
            this.salirBtn.Size = new System.Drawing.Size(75, 23);
            this.salirBtn.TabIndex = 0;
            this.salirBtn.Text = "Salir";
            this.salirBtn.UseVisualStyleBackColor = true;
            this.salirBtn.Click += new System.EventHandler(this.salirBtn_Click);
            // 
            // juegosJugadosChart
            // 
            chartArea3.Name = "ChartArea1";
            this.juegosJugadosChart.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.juegosJugadosChart.Legends.Add(legend3);
            this.juegosJugadosChart.Location = new System.Drawing.Point(85, 29);
            this.juegosJugadosChart.Name = "juegosJugadosChart";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.juegosJugadosChart.Series.Add(series3);
            this.juegosJugadosChart.Size = new System.Drawing.Size(544, 300);
            this.juegosJugadosChart.TabIndex = 1;
            this.juegosJugadosChart.Text = "chart1";
            // 
            // resultadoLbl
            // 
            this.resultadoLbl.AutoSize = true;
            this.resultadoLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultadoLbl.Location = new System.Drawing.Point(80, 373);
            this.resultadoLbl.Name = "resultadoLbl";
            this.resultadoLbl.Size = new System.Drawing.Size(26, 25);
            this.resultadoLbl.TabIndex = 2;
            this.resultadoLbl.Text = "A";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(418, 236);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 25);
            this.label1.TabIndex = 3;
            // 
            // xmlPathLbl
            // 
            this.xmlPathLbl.AutoSize = true;
            this.xmlPathLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xmlPathLbl.Location = new System.Drawing.Point(98, 453);
            this.xmlPathLbl.Name = "xmlPathLbl";
            this.xmlPathLbl.Size = new System.Drawing.Size(25, 25);
            this.xmlPathLbl.TabIndex = 4;
            this.xmlPathLbl.Text = "B";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(81, 425);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ubicacion del archivo XML:";
            // 
            // JuegoMasJugado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 507);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.xmlPathLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resultadoLbl);
            this.Controls.Add(this.juegosJugadosChart);
            this.Controls.Add(this.salirBtn);
            this.Name = "JuegoMasJugado";
            this.Text = "Juego Mas Jugado";
            this.Load += new System.EventHandler(this.JuegoMasJugado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.juegosJugadosChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button salirBtn;
        private System.Windows.Forms.DataVisualization.Charting.Chart juegosJugadosChart;
        private System.Windows.Forms.Label resultadoLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label xmlPathLbl;
        private System.Windows.Forms.Label label2;
    }
}