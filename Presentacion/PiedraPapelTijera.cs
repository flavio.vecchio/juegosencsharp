﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controlador;

namespace Presentacion
{
    public partial class PiedraPapelTijera : Form
    {
        private PiedraPapelTijeraController controller;

        public PiedraPapelTijera()
        {
            InitializeComponent();
            controller = new PiedraPapelTijeraController(piedraImg, papelImg, tijeraImg, puntosJugadorTb,puntosPcTb,turnoActualTb,ganadorRondaTb,jugadorCmb,salirBtn,nuevoJuegoBtn,terminarJuegoBtn,seleccionPcTb);
            controller.inicializarJuego();
            controller.cargarCombo();
        }

        private void salirBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PiedraPapelTijera_Load(object sender, EventArgs e)
        {

        }

        private void nuevoJuegoBtn_Click(object sender, EventArgs e)
        {
            try { 
                controller.comenzarJuego();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
}

        private void piedraImg_Click(object sender, EventArgs e)
        {
            controller.jugadorJugo("piedra");
        }

        private void papelImg_Click(object sender, EventArgs e)
        {
            controller.jugadorJugo("papel");
        }

        private void tijeraImg_Click(object sender, EventArgs e)
        {
            controller.jugadorJugo("tijera");
        }

        private void terminarJuegoBtn_Click(object sender, EventArgs e)
        {
            controller.terminarJuego();
        }
    }
}
