﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Controlador;
using EBL;

namespace Presentacion
{
    public partial class HistorialJuegos : Form
    {
        private InformeController controller;
        public HistorialJuegos()
        {
            InitializeComponent();
            controller = new InformeController();

            historialTaTeTiChart.Palette = ChartColorPalette.EarthTones;
            historialTaTeTiChart.Titles.Add("Historial TA TE TI");

            historialPiedraPapelTijeraChart.Palette = ChartColorPalette.EarthTones;
            historialPiedraPapelTijeraChart.Titles.Add("Historial Piedra Papel Tijera");

        }

        private void HistorialJuegos_Load(object sender, EventArgs e)
        {
            controller.llenarCombo(jugadorCmb);

        }

        private void jugadorCmb_Click(object sender, EventArgs e)
        {
           

        }

        private void salirBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void jugadorCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            Jugador jugador = (Jugador)jugadorCmb.SelectedValue;

            if (jugador == null) return;
            if (controller.noActuar) return;

            int juegosGanadosTaTeTi = controller.cantidadDeJuegos(jugador, "TaTeTi", "ganados");
            int juegosEmpatadosTaTeTi = controller.cantidadDeJuegos(jugador, "TaTeTi", "empatados");
            int juegosPerdidosTaTeTi = controller.cantidadDeJuegos(jugador, "TaTeTi", "perdidos");

            int juegosGanadosPPT = controller.cantidadDeJuegos(jugador, "PiedraPapelTijera", "ganados");
            int juegosEmpatadosPPT = controller.cantidadDeJuegos(jugador, "PiedraPapelTijera", "empatados");
            int juegosPerdidosPPT = controller.cantidadDeJuegos(jugador, "PiedraPapelTijera", "perdidos");

            string[] seriesArrayPPT = { "Ganados", "Empatados", "Perdidos" };
            string[] seriesArrayTTT = { "Ganados", "Empatados", "Perdidos" };

           
            historialTaTeTiChart.Series.Clear();

            Series seriesTaTeTi = historialTaTeTiChart.Series.Add("Total");
            seriesTaTeTi.Points.AddXY(seriesArrayTTT[0], juegosGanadosTaTeTi);
            seriesTaTeTi.Points.AddXY(seriesArrayTTT[1], juegosEmpatadosTaTeTi);
            seriesTaTeTi.Points.AddXY(seriesArrayTTT[2], juegosPerdidosTaTeTi);

           
            historialPiedraPapelTijeraChart.Series.Clear();

            Series seriesPPT = historialPiedraPapelTijeraChart.Series.Add("Total");
            seriesPPT.Points.AddXY(seriesArrayPPT[0], juegosGanadosPPT);
            seriesPPT.Points.AddXY(seriesArrayPPT[1], juegosEmpatadosPPT);
            seriesPPT.Points.AddXY(seriesArrayPPT[2], juegosPerdidosPPT);
        }
    }
}
