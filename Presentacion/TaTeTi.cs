﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controlador;

namespace Presentacion
{
    public partial class TaTeTi : Form
    {
        private TaTeTiController controller;

        public TaTeTi()
        {
            InitializeComponent();
            controller = new TaTeTiController(celda11,celda12,celda13, celda21, celda22, celda23, celda31, celda32, celda33, puntosJugadorTb, puntosPcTb, turnoActualTb, jugadorCmb, salirBtn, nuevoJuegoBtn, terminarJuegoBtn);
            controller.inicializarJuego();
            controller.cargarCombo();
        }

        private void salirBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void nuevoJuegoBtn_Click(object sender, EventArgs e)
        {
            try
            {
                controller.comenzarJuego();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }

        private void celda11_Click(object sender, EventArgs e)
        {
            marcar(celda11);

        }

        private void setXImageTo(PictureBox celda)
        {
            setImageTo(celda, Properties.Resources.tictactoeX);
        }

        private void setOImageTo(PictureBox celda)
        {
            setImageTo(celda, Properties.Resources.tictactoeO);
        }

        private void setImageTo(PictureBox celda, Bitmap image)
        {
            if (celda != null)
            {
                celda.Image = image;
                celda.Refresh();
            }
        }
        private void celda12_Click(object sender, EventArgs e)
        {
            
            marcar(celda12);
        }

        private void marcar(PictureBox celdaSeleccionada)
        {
            setXImageTo(celdaSeleccionada);
            PictureBox celdaJugadaPorPC = controller.jugadorHumanoJugoCelda(celdaSeleccionada);
            setOImageTo(celdaJugadaPorPC);
            controller.analizarEstadoJuego();
            controller.accionSobreEstado();
            
        }

        private void celda13_Click(object sender, EventArgs e)
        {
            marcar(celda13);
        }

        private void celda21_Click(object sender, EventArgs e)
        {
            marcar(celda21);
        }

        private void celda22_Click(object sender, EventArgs e)
        {
            marcar(celda22);
        }

        private void celda23_Click(object sender, EventArgs e)
        {
            marcar(celda23);
        }

        private void celda31_Click(object sender, EventArgs e)
        {
            marcar(celda31);
        }

        private void celda32_Click(object sender, EventArgs e)
        {
            marcar(celda32);
        }

        private void celda33_Click(object sender, EventArgs e)
        {
            marcar(celda33);
        }

        private void terminarJuegoBtn_Click(object sender, EventArgs e)
        {
            controller.terminarJuego();
        }

        private void warGamesMode_Click(object sender, EventArgs e)
        {

            controller.comenzarJuegoEnModoWarGames();
            int cantidadDeJuegos = 10;
            for (int i = 0; i < cantidadDeJuegos; i++)
            {
                jugarModoWarGames();
            }
            controller.terminarJuegoEnModoWarGames();
            
        }

        private void jugarModoWarGames()
        {
            PictureBox celdaJugadaPorPC;
            while (controller.estado == "seguirJugando")
            {
                celdaJugadaPorPC = controller.jugadorPCJugoCelda();
                setXImageTo(celdaJugadaPorPC);

                celdaJugadaPorPC = controller.jugadorPCJugoCelda();
                setOImageTo(celdaJugadaPorPC);

                controller.analizarEstadoJuego();
            }
            controller.accionSobreEstado();
        }
    }
}
