﻿namespace Presentacion
{
    partial class Main
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.presupuestosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearPresupuestoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarPresupuestoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.totalPresupuestadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.totalPresupuestadoPorClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.administracionToolStripMenuItem,
            this.presupuestosToolStripMenuItem,
            this.informesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(990, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // administracionToolStripMenuItem
            // 
            this.administracionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clientesToolStripMenuItem});
            this.administracionToolStripMenuItem.Name = "administracionToolStripMenuItem";
            this.administracionToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.administracionToolStripMenuItem.Text = "Administracion";
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.clientesToolStripMenuItem.Text = "Jugadores";
            this.clientesToolStripMenuItem.Click += new System.EventHandler(this.clientesToolStripMenuItem_Click);
            // 
            // presupuestosToolStripMenuItem
            // 
            this.presupuestosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearPresupuestoToolStripMenuItem,
            this.editarPresupuestoToolStripMenuItem});
            this.presupuestosToolStripMenuItem.Name = "presupuestosToolStripMenuItem";
            this.presupuestosToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.presupuestosToolStripMenuItem.Text = "Juegos";
            // 
            // crearPresupuestoToolStripMenuItem
            // 
            this.crearPresupuestoToolStripMenuItem.Name = "crearPresupuestoToolStripMenuItem";
            this.crearPresupuestoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.crearPresupuestoToolStripMenuItem.Text = "Piedra Papel Tijera";
            this.crearPresupuestoToolStripMenuItem.Click += new System.EventHandler(this.crearPresupuestoToolStripMenuItem_Click);
            // 
            // editarPresupuestoToolStripMenuItem
            // 
            this.editarPresupuestoToolStripMenuItem.Name = "editarPresupuestoToolStripMenuItem";
            this.editarPresupuestoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.editarPresupuestoToolStripMenuItem.Text = "Ta Te Ti";
            this.editarPresupuestoToolStripMenuItem.Click += new System.EventHandler(this.editarPresupuestoToolStripMenuItem_Click);
            // 
            // informesToolStripMenuItem
            // 
            this.informesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.totalPresupuestadoToolStripMenuItem,
            this.totalPresupuestadoPorClienteToolStripMenuItem});
            this.informesToolStripMenuItem.Name = "informesToolStripMenuItem";
            this.informesToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.informesToolStripMenuItem.Text = "Informes";
            // 
            // totalPresupuestadoToolStripMenuItem
            // 
            this.totalPresupuestadoToolStripMenuItem.Name = "totalPresupuestadoToolStripMenuItem";
            this.totalPresupuestadoToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.totalPresupuestadoToolStripMenuItem.Text = "Juego mas jugado";
            this.totalPresupuestadoToolStripMenuItem.Click += new System.EventHandler(this.totalPresupuestadoToolStripMenuItem_Click);
            // 
            // totalPresupuestadoPorClienteToolStripMenuItem
            // 
            this.totalPresupuestadoPorClienteToolStripMenuItem.Name = "totalPresupuestadoPorClienteToolStripMenuItem";
            this.totalPresupuestadoPorClienteToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.totalPresupuestadoPorClienteToolStripMenuItem.Text = "Historial de jugadores por juego";
            this.totalPresupuestadoPorClienteToolStripMenuItem.Click += new System.EventHandler(this.totalPresupuestadoPorClienteToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 583);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.Name = "Main";
            this.Text = "Juegos - Vecchio, Flavio";
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administracionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem presupuestosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearPresupuestoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarPresupuestoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalPresupuestadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalPresupuestadoPorClienteToolStripMenuItem;
    }
}

