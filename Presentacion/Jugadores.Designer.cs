﻿namespace Presentacion
{
    partial class Jugadores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fechaNacimientoDp = new System.Windows.Forms.DateTimePicker();
            this.lblFechaNacimiento = new System.Windows.Forms.Label();
            this.dniTb = new System.Windows.Forms.TextBox();
            this.lblDni = new System.Windows.Forms.Label();
            this.apellidoTb = new System.Windows.Forms.TextBox();
            this.lblApellido = new System.Windows.Forms.Label();
            this.nuevoBn = new System.Windows.Forms.Button();
            this.guardarBn = new System.Windows.Forms.Button();
            this.salirBn = new System.Windows.Forms.Button();
            this.borrarBn = new System.Windows.Forms.Button();
            this.jugadoresDv = new System.Windows.Forms.DataGridView();
            this.lblGrilla = new System.Windows.Forms.Label();
            this.nombreTb = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.idTb = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.localidadTb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.emailTb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.jugadoresDv)).BeginInit();
            this.SuspendLayout();
            // 
            // fechaNacimientoDp
            // 
            this.fechaNacimientoDp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fechaNacimientoDp.Location = new System.Drawing.Point(123, 118);
            this.fechaNacimientoDp.Name = "fechaNacimientoDp";
            this.fechaNacimientoDp.Size = new System.Drawing.Size(121, 20);
            this.fechaNacimientoDp.TabIndex = 74;
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.AutoSize = true;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(16, 122);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(106, 13);
            this.lblFechaNacimiento.TabIndex = 72;
            this.lblFechaNacimiento.Text = "Fecha de nacimiento";
            // 
            // dniTb
            // 
            this.dniTb.Location = new System.Drawing.Point(63, 88);
            this.dniTb.Name = "dniTb";
            this.dniTb.Size = new System.Drawing.Size(100, 20);
            this.dniTb.TabIndex = 71;
            // 
            // lblDni
            // 
            this.lblDni.AutoSize = true;
            this.lblDni.Location = new System.Drawing.Point(16, 91);
            this.lblDni.Name = "lblDni";
            this.lblDni.Size = new System.Drawing.Size(23, 13);
            this.lblDni.TabIndex = 70;
            this.lblDni.Text = "Dni";
            // 
            // apellidoTb
            // 
            this.apellidoTb.Location = new System.Drawing.Point(63, 62);
            this.apellidoTb.Name = "apellidoTb";
            this.apellidoTb.Size = new System.Drawing.Size(293, 20);
            this.apellidoTb.TabIndex = 69;
            // 
            // lblApellido
            // 
            this.lblApellido.AutoSize = true;
            this.lblApellido.Location = new System.Drawing.Point(16, 65);
            this.lblApellido.Name = "lblApellido";
            this.lblApellido.Size = new System.Drawing.Size(44, 13);
            this.lblApellido.TabIndex = 68;
            this.lblApellido.Text = "Apellido";
            // 
            // nuevoBn
            // 
            this.nuevoBn.Location = new System.Drawing.Point(440, 16);
            this.nuevoBn.Name = "nuevoBn";
            this.nuevoBn.Size = new System.Drawing.Size(75, 23);
            this.nuevoBn.TabIndex = 67;
            this.nuevoBn.Text = "Nuevo";
            this.nuevoBn.UseVisualStyleBackColor = true;
            this.nuevoBn.Click += new System.EventHandler(this.nuevoBn_Click);
            // 
            // guardarBn
            // 
            this.guardarBn.Location = new System.Drawing.Point(521, 16);
            this.guardarBn.Name = "guardarBn";
            this.guardarBn.Size = new System.Drawing.Size(75, 23);
            this.guardarBn.TabIndex = 66;
            this.guardarBn.Text = "Guardar";
            this.guardarBn.UseVisualStyleBackColor = true;
            this.guardarBn.Click += new System.EventHandler(this.guardarBn_Click);
            // 
            // salirBn
            // 
            this.salirBn.Location = new System.Drawing.Point(602, 59);
            this.salirBn.Name = "salirBn";
            this.salirBn.Size = new System.Drawing.Size(75, 23);
            this.salirBn.TabIndex = 65;
            this.salirBn.Text = "Salir";
            this.salirBn.UseVisualStyleBackColor = true;
            this.salirBn.Click += new System.EventHandler(this.salirBn_Click);
            // 
            // borrarBn
            // 
            this.borrarBn.Location = new System.Drawing.Point(602, 16);
            this.borrarBn.Name = "borrarBn";
            this.borrarBn.Size = new System.Drawing.Size(75, 23);
            this.borrarBn.TabIndex = 64;
            this.borrarBn.Text = "Borrar";
            this.borrarBn.UseVisualStyleBackColor = true;
            this.borrarBn.Click += new System.EventHandler(this.borrarBn_Click);
            // 
            // jugadoresDv
            // 
            this.jugadoresDv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.jugadoresDv.Location = new System.Drawing.Point(19, 220);
            this.jugadoresDv.MultiSelect = false;
            this.jugadoresDv.Name = "jugadoresDv";
            this.jugadoresDv.ReadOnly = true;
            this.jugadoresDv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.jugadoresDv.Size = new System.Drawing.Size(658, 150);
            this.jugadoresDv.TabIndex = 63;
            this.jugadoresDv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.jugadoresDv_CellClick);
            // 
            // lblGrilla
            // 
            this.lblGrilla.AutoSize = true;
            this.lblGrilla.Location = new System.Drawing.Point(16, 204);
            this.lblGrilla.Name = "lblGrilla";
            this.lblGrilla.Size = new System.Drawing.Size(56, 13);
            this.lblGrilla.TabIndex = 62;
            this.lblGrilla.Text = "Jugadores";
            // 
            // nombreTb
            // 
            this.nombreTb.Location = new System.Drawing.Point(63, 39);
            this.nombreTb.Name = "nombreTb";
            this.nombreTb.Size = new System.Drawing.Size(293, 20);
            this.nombreTb.TabIndex = 61;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(16, 42);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(44, 13);
            this.lblNombre.TabIndex = 60;
            this.lblNombre.Text = "Nombre";
            // 
            // idTb
            // 
            this.idTb.Location = new System.Drawing.Point(63, 13);
            this.idTb.Name = "idTb";
            this.idTb.ReadOnly = true;
            this.idTb.Size = new System.Drawing.Size(181, 20);
            this.idTb.TabIndex = 59;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(16, 16);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(16, 13);
            this.lblId.TabIndex = 58;
            this.lblId.Text = "Id";
            // 
            // localidadTb
            // 
            this.localidadTb.Location = new System.Drawing.Point(75, 173);
            this.localidadTb.Name = "localidadTb";
            this.localidadTb.Size = new System.Drawing.Size(281, 20);
            this.localidadTb.TabIndex = 78;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 176);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 77;
            this.label1.Text = "Localidad";
            // 
            // emailTb
            // 
            this.emailTb.Location = new System.Drawing.Point(63, 150);
            this.emailTb.Name = "emailTb";
            this.emailTb.Size = new System.Drawing.Size(293, 20);
            this.emailTb.TabIndex = 76;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 75;
            this.label2.Text = "Email";
            // 
            // Jugadores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 389);
            this.Controls.Add(this.localidadTb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.emailTb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fechaNacimientoDp);
            this.Controls.Add(this.lblFechaNacimiento);
            this.Controls.Add(this.dniTb);
            this.Controls.Add(this.lblDni);
            this.Controls.Add(this.apellidoTb);
            this.Controls.Add(this.lblApellido);
            this.Controls.Add(this.nuevoBn);
            this.Controls.Add(this.guardarBn);
            this.Controls.Add(this.salirBn);
            this.Controls.Add(this.borrarBn);
            this.Controls.Add(this.jugadoresDv);
            this.Controls.Add(this.lblGrilla);
            this.Controls.Add(this.nombreTb);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.idTb);
            this.Controls.Add(this.lblId);
            this.Name = "Jugadores";
            this.Text = "Jugador";
            this.Load += new System.EventHandler(this.Jugadores_Load);
            ((System.ComponentModel.ISupportInitialize)(this.jugadoresDv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker fechaNacimientoDp;
        private System.Windows.Forms.Label lblFechaNacimiento;
        private System.Windows.Forms.TextBox dniTb;
        private System.Windows.Forms.Label lblDni;
        private System.Windows.Forms.TextBox apellidoTb;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Button nuevoBn;
        private System.Windows.Forms.Button guardarBn;
        private System.Windows.Forms.Button salirBn;
        private System.Windows.Forms.Button borrarBn;
        private System.Windows.Forms.DataGridView jugadoresDv;
        private System.Windows.Forms.Label lblGrilla;
        private System.Windows.Forms.TextBox nombreTb;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox idTb;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox localidadTb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox emailTb;
        private System.Windows.Forms.Label label2;
    }
}