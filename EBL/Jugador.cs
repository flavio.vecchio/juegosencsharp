﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBL
{
    public class Jugador : Top
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Localidad { get; set; }
        public string email { get; set; }

        public Jugador(string id, string nombre, string apellido, string dni, string fechaNacimiento, string localidad, string email)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
            Nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
            Apellido = apellido ?? throw new ArgumentNullException(nameof(apellido));
            Dni = dni ?? throw new ArgumentNullException(nameof(dni));
            FechaNacimiento = DateTime.Parse(fechaNacimiento);
            Localidad = localidad ?? throw new ArgumentNullException(nameof(localidad));
            this.email = email ?? throw new ArgumentNullException(nameof(email));
        }

        public string NombreYApellido { get
            {
                return Nombre + " " + Apellido;
            }
        }
    }
}
