﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EBL;
using Persistencia;


namespace BLL
{
    public class JugadorBLL : Top
    {
        public List<Jugador> jugadores()
        {
            return PersistentAdapter.getInstance().jugadores();
        }

        public void grabar(Jugador objActual)
        {
            if (objActual.Id.Length == 0)
            {
                PersistentAdapter.getInstance().grabarJugadorNuevo(objActual);
            }
            else
            {
                PersistentAdapter.getInstance().modificarJugador(objActual);
            }
        }

        public Jugador obtenerObjetoConId(string id)
        {
            return PersistentAdapter.getInstance().obtenerJugadorConId(id);
        }

        public void borrar(Jugador objActual)
        {
            PersistentAdapter.getInstance().borrarJugador(objActual);
        }

        public void validarDatosDeJugador(Jugador objActual)
        {
            if (objActual.Nombre.Length == 0) throw new Exception("Debe ingresar un nombre");
            if (objActual.Apellido.Length == 0) throw new Exception("Debe ingresar un apellido");
            if (objActual.Localidad.Length == 0) throw new Exception("Debe ingresar una localidad");
            if (objActual.Dni.Length == 0) throw new Exception("Debe ingresar un dni");
            if (objActual.email.Length == 0) throw new Exception("Debe ingresar un email");

            validarFormatoNumerico(objActual.Dni, "El DNI debe tener formato numerico");
            validarFormatoEmail(objActual.email, "El EMAIL debe tener formato aaa@aaa.aaa");
            validarLargo8Numeros(objActual.Dni, "El DNI debe tener como minimo 8 numeros, completar con cero");

            validarNumeroDeDNI(objActual);
        }

        private void validarNumeroDeDNI(Jugador objActual)
        {
            PersistentAdapter p = PersistentAdapter.getInstance();
            if (p.existeOtroJugadorConDNIDe(objActual))
            {
                throw new Exception("Ya existe otro jugador con ese numero de DNI");
            }
        }
    }
}
