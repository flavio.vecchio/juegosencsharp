﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EBL;
using Persistencia;

namespace BLL
{
    public class PiedraPapelTijeraBLL : Top
    {
        

        public List<Jugador> obtenerJugadores()
        {
            return (new JugadorBLL()).jugadores();
        }

        public string jugarPC()
        {
            System.Threading.Thread.Sleep(2000);

            string resultado="";
            Random rnd = new Random();
            int seleccion = rnd.Next(1, 3);
            switch (seleccion)
            {
                case 1:
                    resultado = "piedra";
                    break;
                case 2:
                    resultado = "papel";
                    break;
                case 3:
                    resultado = "tijera";
                    break;
            }

            return resultado;

        }

        public string determinarGanador(string jugadaHumano, string jugadaPC)
        {
            string ganador = "";

            if (jugadaHumano == "piedra" && jugadaPC == "piedra") ganador = "";
            if (jugadaHumano == "piedra" && jugadaPC == "papel") ganador = "pc";
            if (jugadaHumano == "piedra" && jugadaPC == "tijera") ganador = "humano";

            if (jugadaHumano == "papel" && jugadaPC == "piedra") ganador = "humano";
            if (jugadaHumano == "papel" && jugadaPC == "papel") ganador = "";
            if (jugadaHumano == "papel" && jugadaPC == "tijera") ganador = "pc";

            if (jugadaHumano == "tijera" && jugadaPC == "piedra") ganador = "pc";
            if (jugadaHumano == "tijera" && jugadaPC == "papel") ganador = "humano";
            if (jugadaHumano == "tijera" && jugadaPC == "tijera") ganador = "";

            totalDeRondas++;
            return ganador;
        }

        public void recordarJugada(string jugadaHumano, string jugadaPC)
        {
            //No hacemos nada por ahora, puede recordarse los juegos anteriores para hacer mas competitivo el juego
        }

        public string ganadorFinal(int puntosHumano, int puntosPC)
        {
            if (puntosHumano > puntosPC) return "Ganador Humano";
            if (puntosHumano == puntosPC) return "Empate";
            if (puntosHumano < puntosPC) return "Ganador PC";

            return "";
        }

        public void grabarPartida(Jugador jugadorActual, int puntosHumano, int puntosPC)
        {
            PersistentAdapter.getInstance().grabarPartida("PiedraPapelTijera",jugadorActual, puntosHumano, puntosPC,totalDeRondas);
        }

        public void resetearTablero()
        {
            totalDeRondas = 0;
        }
    }
}
