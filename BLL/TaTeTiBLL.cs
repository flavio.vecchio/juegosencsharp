﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EBL;
using Persistencia;


namespace BLL
{
    public class TaTeTiBLL : Top
    {
        private int[,] tablero = new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
        private int movimientos = 0;
        private bool ModoWarGames = false;
        private bool turnoJugador = false;

        public List<Jugador> obtenerJugadores()
        {
            return (new JugadorBLL()).jugadores();
        }

        public void jugadorHumanoJugo(string nombreCelda)
        {
            marcarJugadaEnPosicion(nombreCelda,1);
            movimientos++;

        }

        public void turno(bool turno)
        {
            turnoJugador = turno;
        }
        public string jugadorPCJugo()
        {
            int timeout = 2000;

            if (ModoWarGames) timeout = 150;
            
                
            System.Threading.Thread.Sleep(timeout);

            movimientos++;
            //return estrategiaBruta();
            //return estrategiaBrutaMejorada();
            return estrategiaBrutaMejoradaVersion2();

        }

        private string estrategiaBruta()
        {
            int jugador = 0;
            if (turnoJugador) jugador = 1;
            else jugador = -1;

            for (int x = 0; x <=2 ; x++)
           {
               for (int y = 0; y <=2; y++)
               {
                   if (tablero[x, y] == 0) 
                   {
                       tablero[x, y] = jugador;
                       return x.ToString() + y.ToString();
                   }
               }
           }


            return null;
        }

        public void modoWarGames(bool flag)
        {
            ModoWarGames = flag;
        }

        private string estrategiaBrutaMejorada()
        {
            int jugador = 0;
            if (turnoJugador) jugador = 1;
            else jugador = -1;

            List<Tuple<int, int>> puntosLibres = new List<Tuple<int, int>> { };

            for (int x = 0; x <= 2; x++)
            {
                for (int y = 0; y <= 2; y++)
                {
                    if (tablero[x, y] == 0)
                    {
                        Tuple<int, int> point = Tuple.Create(x, y);
                        puntosLibres.Add(point);
                    }
                }
            }

            if (puntosLibres.Count() == 0) return null;

            if (puntosLibres.Find(p => p.Item1 == 1 && p.Item2 == 1) != null)
            {
                tablero[1, 1] = jugador;
                return "11";
            }

            Random rnd = new Random();
            int cantidadPuntosLibres = puntosLibres.Count();
            int seleccion = rnd.Next(1, cantidadPuntosLibres);
            int coordenadaX = puntosLibres[seleccion - 1].Item1;
            int coordenadaY = puntosLibres[seleccion - 1].Item2;
            
            tablero[coordenadaX, coordenadaY] = jugador;
            return coordenadaX.ToString() + coordenadaY.ToString();

            
        }

        private string estrategiaBrutaMejoradaVersion2()
        {
            int jugador = 0;
            if (turnoJugador) jugador = 1;
                else jugador = -1;

            List<Tuple<int, int>> puntosLibres = new List<Tuple<int, int>> { };

            for (int x = 0; x <= 2; x++)
            {
                for (int y = 0; y <= 2; y++)
                {
                    if (tablero[x, y] == 0)
                    {
                        Tuple<int, int> point = Tuple.Create(x, y);
                        puntosLibres.Add(point);
                    }
                }
            }

            if (puntosLibres.Count() == 0) return null;

            if (puntosLibres.Find(p => p.Item1 == 1 && p.Item2 == 1) != null)
            {
                tablero[1, 1] = jugador;
                return "11";
            }

            List<Tuple<int, int>> posicionesAmenazadas = this.posicionesAmenazadas();
            if (posicionesAmenazadas.Count() == 0)
            {
                return estrategiaBrutaMejorada();
            }

            int coordenadaX = posicionesAmenazadas[0].Item1;
            int coordenadaY = posicionesAmenazadas[0].Item2;

            tablero[coordenadaX, coordenadaY] = jugador;
            return coordenadaX.ToString() + coordenadaY.ToString();

        }

        private List<Tuple<int, int>> posicionesAmenazadas()
        {
            //Esto es feo, se puede mejorar un monton
            List<Tuple<int, int>> posicionesAmenazadas = new List<Tuple<int, int>> { };
            Tuple<int, int> posicionLibre = null;
            int resultado = 0;

            analizarSiHayPosicionAmenazadaEnX(0, posicionesAmenazadas, out posicionLibre, out resultado);
            analizarSiHayPosicionAmenazadaEnX(1, posicionesAmenazadas, out posicionLibre, out resultado);
            analizarSiHayPosicionAmenazadaEnX(2, posicionesAmenazadas, out posicionLibre, out resultado);

            analizarSiHayPosicionAmenazadaEnY(0, posicionesAmenazadas, out posicionLibre, out resultado);
            analizarSiHayPosicionAmenazadaEnY(1, posicionesAmenazadas, out posicionLibre, out resultado);
            analizarSiHayPosicionAmenazadaEnY(2, posicionesAmenazadas, out posicionLibre, out resultado);

            resultado = tablero[0, 0] + tablero[1, 1] + tablero[2, 2];
            posicionLibre = null;
            if (tablero[0, 0] == 0) posicionLibre = Tuple.Create(0, 0);
            if (tablero[1, 1] == 0) posicionLibre = Tuple.Create(1, 1);
            if (tablero[2, 2] == 0) posicionLibre = Tuple.Create(2, 2);
            if (resultado == 2) posicionesAmenazadas.Add(posicionLibre);

            resultado = tablero[0, 2] + tablero[1, 1] + tablero[2, 0];
            posicionLibre = null;
            if (tablero[0, 2] == 0) posicionLibre = Tuple.Create(0, 2);
            if (tablero[1, 1] == 0) posicionLibre = Tuple.Create(1, 1);
            if (tablero[2, 0] == 0) posicionLibre = Tuple.Create(2, 0);
            if (resultado == 2) posicionesAmenazadas.Add(posicionLibre);

            return posicionesAmenazadas;

        }

        private void analizarSiHayPosicionAmenazadaEnX(int valor,List<Tuple<int, int>> posicionesAmenazadas, out Tuple<int, int> posicionLibre, out int resultado)
        {
            resultado = 0;
            posicionLibre = null;
            for (int i = 0; i <= 2; i++)
            {
                resultado = resultado + tablero[valor, i];
                if (tablero[valor, i] == 0)
                {
                    posicionLibre = Tuple.Create(valor, i);
                }
            }
            if (resultado == 2) posicionesAmenazadas.Add(posicionLibre);
        }

        private void analizarSiHayPosicionAmenazadaEnY(int valor, List<Tuple<int, int>> posicionesAmenazadas, out Tuple<int, int> posicionLibre, out int resultado)
        {
            resultado = 0;
            posicionLibre = null;
            for (int i = 0; i <= 2; i++)
            {
                resultado = resultado + tablero[i, valor];
                if (tablero[i, valor] == 0)
                {
                    posicionLibre = Tuple.Create(i, valor);
                }
            }
            if (resultado == 2) posicionesAmenazadas.Add(posicionLibre);
        }

        public void inicializarRonda()
        {
            totalDeRondas = 0;
        }

        private void marcarJugadaEnPosicion(string nombreCelda,int marcador)
        {
            switch (nombreCelda)
            {
                case "celda11":
                    tablero[0, 0] = marcador;
                    break;
                case "celda12":
                    tablero[0, 1] = marcador;
                    break;
                case "celda13":
                    tablero[0, 2] = marcador;
                    break;
                case "celda21":
                    tablero[1, 0] = marcador;
                    break;
                case "celda22":
                    tablero[1, 1] = marcador;
                    break;
                case "celda23":
                    tablero[1, 2] = marcador;
                    break;
                case "celda31":
                    tablero[2, 0] = marcador;
                    break;
                case "celda32":
                    tablero[2, 1] = marcador;
                    break;
                case "celda33":
                    tablero[2, 2] = marcador;
                    break;
            }
        }

        public string ganadorFinal(int puntosHumano, int puntosPC)
        {
            if (puntosHumano > puntosPC) return "Ganador Humano";
            if (puntosHumano == puntosPC) return "Empate";
            if (puntosHumano < puntosPC) return "Ganador PC";

            return "";
        }

        public void grabarPartida(Jugador jugadorActual, int puntosHumano, int puntosPC)
        {
            PersistentAdapter.getInstance().grabarPartida("TaTeTi", jugadorActual, puntosHumano, puntosPC,totalDeRondas);
        }


        public string analizarEstadoJuego()
        {
            string estado = "seguirJugando";
            if (movimientos >= 9) estado = "empate";
            if (ganoHumano()) estado = "ganadorHumano";
            if (ganoPC()) estado = "ganadorPC";
            if (estado != "seguirJugando") totalDeRondas++;

            return estado;
        }

        private bool ganoHumano()
        {
            return verificarGanador(3);
        }

        private bool ganoPC()
        {
            return verificarGanador(-3);
        }

        private bool verificarGanador(int ganador)
        {
            //linea 00 a 02
            int resultado = 0;

            resultado = 0;
            for (int i = 0; i <= 2; i++)
            {
                resultado = resultado + tablero[0, i];
            }
            if (resultado == ganador) 
                return true;

            //linea 10 a 12
            resultado = 0;
            for (int i = 0; i <= 2; i++)
            {
                resultado = resultado + tablero[1, i];
            }
            if (resultado == ganador) 
                return true;

            //linea 20 a 22
            resultado = 0;
            for (int i = 0; i <= 2; i++)
            {
                resultado = resultado + tablero[2, i];
            }
            if (resultado == ganador) 
                return true;

            //linea 00 a 20
            resultado = 0;
            for (int i = 0; i <= 2; i++)
            {
                resultado = resultado + tablero[i, 0];
            }
            if (resultado == ganador) 
                return true;

            //linea 01 a 12
            resultado = 0;
            for (int i = 0; i <= 2; i++)
            {
                resultado = resultado + tablero[i,1];
            }
            if (resultado == ganador) 
                return true;

            //linea 20 a 22
            resultado = 0;
            for (int i = 0; i <= 2; i++)
            {
                resultado = resultado + tablero[i,2];
            }
            if (resultado == ganador) 
                return true;


            resultado = tablero[0, 0] + tablero[1, 1] + tablero[2, 2];
            if (resultado == ganador) 
                return true;


            resultado = tablero[0,2]+ tablero[1, 1] + tablero[2, 0];
            if (resultado == ganador) 
                return true;



            return false;

        }

        public void resetearTablero()
        {
            tablero = new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
            movimientos = 0;
        }
    }
}
