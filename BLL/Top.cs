﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace BLL
{
    public abstract class Top
    {
        protected int totalDeRondas = 0;
        public void validarFormatoNumerico(string numero, string errorMsg)
        {
            bool esNumerico = Regex.IsMatch(numero, @"^-?[0-9,\.]+$");

            if (!esNumerico)
            {
                throw new Exception(errorMsg);
            }
        }

        public void validarFormatoEmail(string email, string errorMsg)
        {
            bool esEmail = Regex.IsMatch(email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            if (!esEmail)
            {
                throw new Exception(errorMsg);
            }
        }

        public void validarLargo8Numeros(string numero, string errorMsg)
        {
            bool esLargoCorrecto = Regex.IsMatch(numero, @"^[0-9]{8}$");

            if (!esLargoCorrecto)
            {
                throw new Exception(errorMsg);
            }
        }

        

    }
}
