﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Persistencia;

namespace BLL
{
    public class InformeBLL : Top
    {
        public int cantidadDeJuegos(string juego)
        {
            return PersistentAdapter.getInstance().cantidadDeJuegos(juego);
        }

        public string pathXML()
        {
            return PersistentAdapter.getInstance().pathXML();
        }

        public List<EBL.Jugador> obtenerJugadores()
        {
            return PersistentAdapter.getInstance().jugadores();
        }

        public int cantidadDeJuegos(EBL.Jugador jugador, string juego, string tipo)
        {
            return PersistentAdapter.getInstance().cantidadDeJuegos(jugador,juego,tipo);
        }
    }
}
