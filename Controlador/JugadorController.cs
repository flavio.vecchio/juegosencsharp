﻿using EBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;



namespace Controlador
{
    public class JugadorController : Top
    {
        private DataGridView jugadoresDv;
        private TextBox idTb;
        private TextBox nombreTb;
        private TextBox apellidoTb;
        private TextBox dniTb;
        private DateTimePicker fechaNacimientoDp;
        private TextBox emailTb;
        private TextBox localidadTb;

        private Jugador objActual;
        private JugadorBLL bll;

        public JugadorController(DataGridView jugadoresDv, TextBox idTb, TextBox nombreTb, TextBox apellidoTb, TextBox dniTb, DateTimePicker fechaNacimientoDp, TextBox emailTb, TextBox localidadTb)
        {
            this.jugadoresDv = jugadoresDv;
            this.idTb = idTb;
            this.nombreTb = nombreTb;
            this.apellidoTb = apellidoTb;
            this.dniTb = dniTb;
            this.fechaNacimientoDp = fechaNacimientoDp;
            this.emailTb = emailTb;
            this.localidadTb = localidadTb;
            this.bll = new JugadorBLL();
        }

        public void nuevo()
        {
            limpiarCampos();
            string fecha = fechaNacimientoDp.Value.ToString("dd/MM/yyyy");
            objActual = new Jugador(idTb.Text, nombreTb.Text, apellidoTb.Text, dniTb.Text, fecha, localidadTb.Text, emailTb.Text);
            nombreTb.Enabled = true;
            apellidoTb.Enabled = true;
            dniTb.Enabled = true;
            fechaNacimientoDp.Enabled = true;
            localidadTb.Enabled = true;
            emailTb.Enabled = true;
        }

        public void borrar()
        {
            if (objActual == null) throw new Exception("No hay nada para borrar");

            if (sePuedeEliminarObjeto())
                bll.borrar(objActual);
        }

        public void seleccionar()
        {
            string id = obtenerCampoDeDgSeleccionado(jugadoresDv, 0);
            nuevo();
            objActual = bll.obtenerObjetoConId(id);
            idTb.Text = objActual.Id.ToString();
            nombreTb.Text = objActual.Nombre;
            apellidoTb.Text = objActual.Apellido;
            dniTb.Text = objActual.Dni;
            fechaNacimientoDp.Value = objActual.FechaNacimiento;
            localidadTb.Text = objActual.Localidad;
            emailTb.Text = objActual.email;
        }

        public void grabar()
        {
            if (objActual == null) throw new Exception("No hay nada para grabar");

            objActual.Id = idTb.Text;
            objActual.Nombre = nombreTb.Text;
            objActual.Apellido = apellidoTb.Text;
            objActual.Dni = dniTb.Text;
            objActual.FechaNacimiento = fechaNacimientoDp.Value;
            objActual.Localidad = localidadTb.Text;
            objActual.email = emailTb.Text;
            bll.validarDatosDeJugador(objActual);
            bll.grabar(objActual);

        }

        private void limpiarCampos()
        {
            idTb.Text = "";
            nombreTb.Text = "";
            apellidoTb.Text = "";
            dniTb.Text = "";
            localidadTb.Text = "";
            emailTb.Text = "";
            objActual = null;  
        }

        public void refrescarGrilla()
        {
            List<Jugador> jugadores = bll.jugadores();
            refrescarVista(jugadores, jugadoresDv);
        }

        private void refrescarVista(List<Jugador> lista, DataGridView dv)
        {
            int index = this.obtenerIndice(dv);

            var r = (from c in lista
                         select new
                         {
                             Id = c.Id,
                             Nombre = c.Nombre,
                         }).ToList();
            dv.DataSource = null;
            dv.DataSource = r;
            this.seleccionarFilaActual(dv, index, r.Count);
        }

        public void inicializar()
        {
            limpiarCampos();
            objActual = null;
            nombreTb.Enabled = false;
            apellidoTb.Enabled = false;
            dniTb.Enabled = false;
            fechaNacimientoDp.Enabled = false;
            emailTb.Enabled = false;
            localidadTb.Enabled = false;
        }

        
    }
}
