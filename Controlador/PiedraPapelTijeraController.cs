﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using EBL;

namespace Controlador
{
    public class PiedraPapelTijeraController : JuegoController
    {
        private PictureBox piedraImg;
        private PictureBox papelImg;
        private PictureBox tijeraImg;
        private TextBox puntosJugadorTb;
        private TextBox puntosPcTb;
        
        private TextBox ganadorRondaTb;
        private TextBox seleccionoPcTb;
        
        private Button salirBtn;
        private Button nuevoJuegoBtn;
        private Button terminarJuegoBtn;

        

        public PiedraPapelTijeraController(PictureBox piedraImg, PictureBox papelImg, PictureBox tijeraImg, TextBox puntosJugadorTb, TextBox puntosPcTb, TextBox turnoActualTb, TextBox ganadorRondaTb, ComboBox jugadorCmb, Button salirBtn, Button nuevoJuegoBtn, Button terminarJuegoBtn, TextBox seleccionPcTb)
        {
            this.piedraImg = piedraImg;
            this.papelImg = papelImg;
            this.tijeraImg = tijeraImg;
            this.puntosJugadorTb = puntosJugadorTb;
            this.puntosPcTb = puntosPcTb;
            this.turnoActualTb = turnoActualTb;
            this.ganadorRondaTb = ganadorRondaTb;
            this.jugadorCmb = jugadorCmb;
            this.salirBtn = salirBtn;
            this.nuevoJuegoBtn = nuevoJuegoBtn;
            this.terminarJuegoBtn = terminarJuegoBtn;
            this.seleccionoPcTb = seleccionPcTb;

            bll = new PiedraPapelTijeraBLL();
        }

        public void terminarJuego()
        {
            declararGanador();
            grabarPartida();
            inicializarJuego();
        }

        private void grabarPartida()
        {
            bll.grabarPartida(jugadorActual, int.Parse(puntosJugadorTb.Text), int.Parse(puntosPcTb.Text));
        }

        private void declararGanador()
        {
            string ganador = bll.ganadorFinal(int.Parse(puntosJugadorTb.Text), int.Parse(puntosPcTb.Text));
            MessageBox.Show("Resultado final: "+ganador);
        }

        public void jugadorJugo(string jugadaHumano )
        {
            cambiarTurno();
            mostrarTurno();
            string jugadaPC = bll.jugarPC();
            seleccionoPcTb.Text = jugadaPC;
            analizarJugada(jugadaHumano, jugadaPC);
            bll.recordarJugada(jugadaHumano, jugadaPC);
            cambiarTurno();
            mostrarTurno();
        }

        private void analizarJugada(string jugadaHumano, string jugadaPC)
        {
            string ganador=bll.determinarGanador(jugadaHumano, jugadaPC);
            if (ganador == "humano") indicarGanadorHumano();
            if (ganador == "pc") indicarGanadorPC();
            if (ganador == "") indicarEmpate();
            
        }

        private void indicarEmpate()
        {
            ganadorRondaTb.Text = "Empate";
        }

        private void indicarGanadorPC()
        {
            puntosPcTb.Text = (int.Parse(puntosPcTb.Text) + 1).ToString();
            ganadorRondaTb.Text = "PC";
        }

        private void indicarGanadorHumano()
        {
            puntosJugadorTb.Text = (int.Parse(puntosJugadorTb.Text) + 1).ToString();
            ganadorRondaTb.Text = "Jugador 1";
        }

        private void cambiarTurno()
        {
            if (turnoJugador)
            {
                turnoJugador = false;
                piedraImg.Enabled = false;
                papelImg.Enabled = false;
                tijeraImg.Enabled = false;
            }
            else
            {
                turnoJugador = true;
                piedraImg.Enabled = true;
                papelImg.Enabled = true;
                tijeraImg.Enabled = true;
            }
        }

        public void comenzarJuego()
        {
            if (jugadorCmb.SelectedItem == null) throw new Exception("Debe seleccionar un jugador");

            jugadorActual = (Jugador) jugadorCmb.SelectedItem;

            jugadorCmb.Enabled = false;
            nuevoJuegoBtn.Enabled = false;
            salirBtn.Enabled = false;
            terminarJuegoBtn.Enabled = true;
            mostrarTurno();
            piedraImg.Enabled = true;
            papelImg.Enabled = true;
            tijeraImg.Enabled = true;
            puntosJugadorTb.Text = "0";
            puntosPcTb.Text = "0";
            ganadorRondaTb.Text = "Mucha suerte...";
            fecha = DateTime.Now;
            bll.resetearTablero();
            
        }

        

        public void inicializarJuego()
        {
            piedraImg.Enabled = false;
            papelImg.Enabled = false;
            tijeraImg.Enabled = false;
            salirBtn.Enabled = true;
            nuevoJuegoBtn.Enabled = true;
            terminarJuegoBtn.Enabled = false;
            puntosJugadorTb.Text = "";
            puntosPcTb.Text = "";
            turnoActualTb.Text = "";
            ganadorRondaTb.Text = "";
            seleccionoPcTb.Text = "";
            jugadorCmb.Enabled = true;
            jugadorActual = null;
            turnoJugador = true;
        }
    }
}
