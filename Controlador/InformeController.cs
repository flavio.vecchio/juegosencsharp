﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using EBL;

namespace Controlador
{
    public class InformeController : Top
    {
        public bool noActuar = false;
        public int cantidadJuegos(string juego)
        {
            return (new InformeBLL()).cantidadDeJuegos(juego);
        }

        public void llenarCombo(ComboBox jugadorCmb)
        {
            noActuar = true;
            List<Jugador> jugadores = (new InformeBLL()).obtenerJugadores();
            jugadorCmb.DataSource = null;
            jugadorCmb.DataSource = jugadores;
            jugadorCmb.DisplayMember = "NombreYApellido";
            jugadorCmb.SelectedIndex = -1;
            noActuar = false;
        }

        public int cantidadDeJuegos(Jugador jugador, string juego, string tipo)
        {
            return (new InformeBLL()).cantidadDeJuegos(jugador, juego, tipo);
        }
    }
}
