﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace Controlador
{
    public abstract class Top
    {
        public string pathXML()
        {
            return (new InformeBLL()).pathXML();
        }
        internal int obtenerIndice(DataGridView dg)
        {
            int index = -1;
            if (dg.CurrentRow != null)
            {
                index = dg.CurrentRow.Index;
            }

            return index;
        }

        internal void seleccionarFilaActual(DataGridView dg, int index, int count)
        {
            if (count < index - 1) { index = -1; }
            if (count == index) { index = index - 1; }
            if (count == 0) { index = -1; }
            if (index >= 0)
            {
                dg.CurrentCell = dg[0, index];
            }
        }

        virtual public string obtenerCampoDeDgSeleccionado(DataGridView dg, int campo)
        {
            if (dg.SelectedRows.Count == 0) return "";

            DataGridViewRow row = dg.SelectedRows[0];
            string result = row.Cells[campo].Value.ToString();

            return result;
        }

        internal void validarSeleccion(string valor)
        {
            if (valor.Equals(""))
            {
                throw new Exception("No hay nada seleccionado");
            }
        }

        internal bool sePuedeEliminarObjeto()
        {
            DialogResult dialogResult = MessageBox.Show("Va a eliminar el objeto, esta seguro?", "WARNING - Eliminacion", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                return true;
            }
            return false;
        }
    }
}

