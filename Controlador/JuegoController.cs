﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using EBL;

namespace Controlador
{
    public abstract class JuegoController: Top
    {
        protected dynamic bll;
        protected Jugador jugadorActual;
        protected bool turnoJugador;
        protected DateTime fecha;

        protected ComboBox jugadorCmb;
        protected TextBox turnoActualTb;
        protected bool modoWarGames = false;

        protected void mostrarTurno()
        {
            if (turnoJugador && !modoWarGames)
            {
                turnoActualTb.Text = "Jugador";
                turnoActualTb.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                turnoActualTb.Text = "PC";
                turnoActualTb.BackColor = System.Drawing.Color.Yellow;
            }
            turnoActualTb.Refresh();

        }

        public void cargarCombo()
        {
            List<Jugador> jugadores = bll.obtenerJugadores();
            jugadorCmb.DataSource = null;
            jugadorCmb.DataSource = jugadores;
            jugadorCmb.DisplayMember = "NombreYApellido";
            jugadorCmb.SelectedIndex = -1;

        }
    }
}
