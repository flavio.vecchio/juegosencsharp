﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using EBL;

namespace Controlador
{
    public class TaTeTiController : JuegoController
    {
        private PictureBox celda11;
        private PictureBox celda12;
        private PictureBox celda13;
        private PictureBox celda21;
        private PictureBox celda22;
        private PictureBox celda23;
        private PictureBox celda31;
        private PictureBox celda32;
        private PictureBox celda33;
        private TextBox puntosJugadorTb;
        private TextBox puntosPcTb;
       
        private Button salirBtn;
        private Button nuevoJuegoBtn;
        private Button terminarJuegoBtn;
        public string estado;
       

        

       

        public TaTeTiController(PictureBox celda11, PictureBox celda12, PictureBox celda13, PictureBox celda21, PictureBox celda22, PictureBox celda23, PictureBox celda31, PictureBox celda32, PictureBox celda33, TextBox puntosJugadorTb, TextBox puntosPcTb, TextBox turnoActualTb, ComboBox jugadorCmb, Button salirBtn, Button nuevoJuegoBtn, Button terminarJuegoBtn)
        {
            this.celda11 = celda11;
            this.celda12 = celda12;
            this.celda13 = celda13;
            this.celda21 = celda21;
            this.celda22 = celda22;
            this.celda23 = celda23;
            this.celda31 = celda31;
            this.celda32 = celda32;
            this.celda33 = celda33;
            this.puntosJugadorTb = puntosJugadorTb;
            this.puntosPcTb = puntosPcTb;
            this.turnoActualTb = turnoActualTb;
            this.jugadorCmb = jugadorCmb;
            this.salirBtn = salirBtn;
            this.nuevoJuegoBtn = nuevoJuegoBtn;
            this.terminarJuegoBtn = terminarJuegoBtn;

            bll = new TaTeTiBLL();
        }

        public PictureBox jugadorHumanoJugoCelda(PictureBox celda)
        {
            celda.Enabled = false;
            bll.jugadorHumanoJugo(celda.Name);
            cambiarTurno();
            string posicion = bll.jugadorPCJugo();
            cambiarTurno();
            PictureBox celdaJugadaPorPC = obtenerCeldaDePosicion(posicion);
            if(celdaJugadaPorPC!=null)
                celdaJugadaPorPC.Enabled =  false;
            return celdaJugadaPorPC;

        }

        public void analizarEstadoJuego()
        {
            estado = bll.analizarEstadoJuego();
            
        }

        public void accionSobreEstado()
        {
            if (estado == "ganadorHumano") { indicarGanadorHumano(); };
            if (estado == "empate") { indicarEmpate(); };
            if (estado == "ganadorPC") { indicarGanadorPC(); };
            if (estado != "seguirJugando") resetearTablero();
        }

        private void indicarEmpate()
        {
            if (!modoWarGames) MessageBox.Show("Empatamos, ¿Jugamos revancha?");
        }

        private void indicarGanadorPC()
        {
            puntosPcTb.Text = (int.Parse(puntosPcTb.Text) + 1).ToString();
            puntosPcTb.Refresh();
            if(!modoWarGames) MessageBox.Show("GANE!!!");
        }

        private void indicarGanadorHumano()
        {
            puntosJugadorTb.Text = (int.Parse(puntosJugadorTb.Text) + 1).ToString();
            puntosJugadorTb.Refresh();
            if (!modoWarGames) MessageBox.Show("Me ganaste :(");
        }

        public void comenzarJuegoEnModoWarGames()
        {
            jugadorCmb.SelectedIndex = -1;
            jugadorCmb.Refresh();
            jugadorActual = null;
            modoWarGames = true;
            bll.modoWarGames(true);
            comenzarJuego();
        }

        public void terminarJuegoEnModoWarGames()
        {
            modoWarGames = false;
            terminarJuego();
            bll.modoWarGames(false);
        }
        public PictureBox jugadorPCJugoCelda()
        {
            cambiarTurno();
            mostrarTurno();
            bll.turno(turnoJugador);
            string posicion = bll.jugadorPCJugo();
            PictureBox celdaJugadaPorPC = obtenerCeldaDePosicion(posicion);
            if (celdaJugadaPorPC != null)
                celdaJugadaPorPC.Enabled = false;
            return celdaJugadaPorPC;
        }

        public void resetearTablero()
        {
            resetearCelda(celda11);
            resetearCelda(celda12);
            resetearCelda(celda13);
            resetearCelda(celda21);
            resetearCelda(celda22);
            resetearCelda(celda23);
            resetearCelda(celda31);
            resetearCelda(celda32);
            resetearCelda(celda33);
            estado = "seguirJugando";
            bll.resetearTablero();
        }

        private void resetearCelda(PictureBox unaCelda)
        {
            unaCelda.Enabled = true;
            if (unaCelda.Image != null) unaCelda.Image.Dispose(); 
            unaCelda.Image = null;
            unaCelda.Refresh();
        }
        public void terminarJuego()
        {
            declararGanador();
            grabarPartida();
            inicializarJuego();
        }

        private void grabarPartida()
        {
            bll.grabarPartida(jugadorActual, int.Parse(puntosJugadorTb.Text), int.Parse(puntosPcTb.Text));
        }

        private void declararGanador()
        {
            string ganador = bll.ganadorFinal(int.Parse(puntosJugadorTb.Text), int.Parse(puntosPcTb.Text));
            MessageBox.Show("Resultado final: " + ganador);
        }

        private PictureBox obtenerCeldaDePosicion(string posicion)
        {
            switch (posicion)
            {
                case "00":
                    return celda11;
                case "01":
                    return celda12;
                case "02":
                    return celda13;
                case "10":
                    return celda21;
                case "11":
                    return celda22;
                case "12":
                    return celda23;
                case "20":
                    return celda31;
                case "21":
                    return celda32;
                case "22":
                    return celda33;
            }

            return null;
        }

        public void inicializarJuego()
        {
            celda11.Enabled = false;
            celda12.Enabled = false;
            celda13.Enabled = false;
            celda21.Enabled = false;
            celda22.Enabled = false;
            celda23.Enabled = false;
            celda31.Enabled = false;
            celda32.Enabled = false;
            celda33.Enabled = false;

            salirBtn.Enabled = true;
            nuevoJuegoBtn.Enabled = true;
            terminarJuegoBtn.Enabled = false;
            puntosJugadorTb.Text = "";
            puntosPcTb.Text = "";
            turnoActualTb.Text = "";
            
            jugadorCmb.Enabled = true;
            jugadorActual = null;
            turnoJugador = true;

            bll.inicializarRonda();
        }

        public void comenzarJuego()
        {
            if (!modoWarGames && jugadorCmb.SelectedItem == null) throw new Exception("Debe seleccionar un jugador");

            jugadorActual = (Jugador)jugadorCmb.SelectedItem;
            resetearTablero();
            jugadorCmb.Enabled = false;
            nuevoJuegoBtn.Enabled = false;
            salirBtn.Enabled = false;
            terminarJuegoBtn.Enabled = true;
            mostrarTurno();
            celda11.Enabled = true;
            celda12.Enabled = true;
            celda13.Enabled = true;
            celda21.Enabled = true;
            celda22.Enabled = true;
            celda23.Enabled = true;
            celda31.Enabled = true;
            celda32.Enabled = true;
            celda33.Enabled = true;
            puntosJugadorTb.Text = "0";
            puntosPcTb.Text = "0";
            
            fecha = DateTime.Now;
            estado = "seguirJugando";

        }

        private void cambiarTurno()
        {
            if (turnoJugador)
            {
                turnoJugador = false;
                
            }
            else
            {
                turnoJugador = true;
                
            }
            bll.turno(turnoJugador);
            mostrarTurno();
        }
    }
}
