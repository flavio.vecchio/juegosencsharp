﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EBL;
using System.Xml.Linq;
using System.IO;

namespace Persistencia
{
    public class PersistentAdapter
    {
        private static PersistentAdapter instance;
        private XDocument document;

        private XElement root;
        private string BaseXMLName = "base.xml";
        private string fullName;

        public string pathXML()
        {
            return fullName;
        }

        public int cantidadDeJuegos(string juego)
        {
            int total=0;

            XElement partidas = obtenerNodoPartidas();
            foreach (var partida in partidas.Elements())
            {
                if(partida.Attribute("juego").Value==juego)
                    total = total + int.Parse(partida.Attribute("totalRondas").Value);
            }

            return total;
        }

        public int cantidadDeJuegos(Jugador jugador, string juego, string tipo)
        {
            int ganados = 0;
            int perdidos = 0;
            int empatados = 0;

            XElement partidas = obtenerNodoPartidas();
            foreach (var partida in partidas.Elements())
            {
                if (partida.Attribute("juego").Value == juego && partida.Attribute("jugadorId").Value == jugador.Id)
                { ganados = ganados + int.Parse(partida.Attribute("puntosHumano").Value);
                  perdidos = perdidos + int.Parse(partida.Attribute("puntosPC").Value);
                  empatados = empatados + (int.Parse(partida.Attribute("totalRondas").Value) - int.Parse(partida.Attribute("puntosHumano").Value) - int.Parse(partida.Attribute("puntosPC").Value));
                }
            }

            if (tipo == "ganados") return ganados;
            if (tipo == "perdidos") return perdidos;
            if (tipo == "empatados") return empatados;

            return -1;
        }

        

        public PersistentAdapter()
        {
            abrirArchivoXML();
        }

        public bool existeOtroJugadorConDNIDe(Jugador objActual)
        {
            XElement jugadores = obtenerNodoJugadores();
            foreach (XElement item in jugadores.Elements())
            {
                if (item.Attribute("Id").Value.ToString() != objActual.Id && item.Attribute("Dni").Value.ToString() == objActual.Dni)
                {
                    return true;
                }
            }

            return false;
        }

        private void abrirArchivoXML()
        {
            string workingDirectory = Environment.CurrentDirectory;
            string projectDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName;
            fullName = projectDirectory + "/" + BaseXMLName;
            if (!File.Exists(fullName))
            {
                XDocument xmlDoc = new XDocument(new XElement("root"));
                xmlDoc.Save(fullName);
            }

            document = XDocument.Load(fullName);
            root = document.Root;

        }

        public Jugador obtenerJugadorConId(string id)
        {
            XElement jugadores = obtenerNodoJugadores();
            
            foreach (XElement item in jugadores.Elements())
            {
                if (item.Attribute("Id").Value.ToString() == id)
                {
                    return crearJugadorAPartirDeElementoXML(item);
                }
            }

            return null;
        }

        private Jugador crearJugadorAPartirDeElementoXML(XElement item)
        {

            string Id = item.Attribute("Id").Value;
            string Nombre = item.Attribute("Nombre").Value;
            string Apellido = item.Attribute("Apellido").Value;
            string DNI = item.Attribute("Dni").Value;
            string FechaNacimiento = item.Attribute("FechaNacimiento").Value;
            string Email = item.Attribute("Email").Value;
            string Localidad = item.Attribute("Localidad").Value;

            return new Jugador(Id, Nombre, Apellido, DNI, FechaNacimiento, Localidad, Email);
        }

        public void borrarJugador(Jugador obj)
        {
            XElement jugadores = obtenerNodoJugadores();
            XElement elementoABorrar=null;
            foreach (XElement item in jugadores.Elements())
            {
                if (item.Attribute("Id").Value.ToString() == obj.Id)
                {
                    elementoABorrar = item;
                    break;
                }
            }

            if (elementoABorrar != null)
            {
                elementoABorrar.Remove();
                grabarArchivoXML();
            }
        }

        public void grabarPartida(string juego, Jugador jugadorActual, int puntosHumano, int puntosPC, int totalDeRondas)
        {
            XElement partida = crearElementoPartidaXML(juego,jugadorActual,puntosHumano,puntosPC,totalDeRondas);
            XElement partidas = obtenerNodoPartidas();
            partidas.Add(partida);
            grabarArchivoXML();
        }

        private XElement crearElementoPartidaXML(string juego, Jugador jugador, int puntosHumano, int puntosPC, int totalDeRondas)
        {
            string id="ModoWarGames";
            if (jugador != null) id = jugador.Id;
            
            XElement elemento = new XElement("partida",
                                               new XAttribute("juego", juego),
                                               new XAttribute("jugadorId", id),
                                               new XAttribute("puntosHumano", puntosHumano),
                                               new XAttribute("puntosPC", puntosPC),
                                               new XAttribute("totalRondas", totalDeRondas)
                               );
            return elemento;
        }

        public void modificarJugador(Jugador obj)
        {
            borrarJugador(obj);
            grabarJugadorNuevo(obj);
        }

        public void grabarJugadorNuevo(Jugador obj)
        {
            if(obj.Id.Length==0) obj.Id = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString();

            XElement jugador = crearElementoJugadorXML(obj);
            XElement jugadores = obtenerNodoJugadores();
            jugadores.Add(jugador);
            grabarArchivoXML();
        }

        

        private XElement obtenerNodoPartidas()
        {
            XElement partidas = root.Element("partidas");
            if (partidas == null)
            {
                partidas = new XElement("partidas");
                root.Add(partidas);
            }

            return partidas;
        }

        private XElement obtenerNodoJugadores()
        {
            XElement jugadores = root.Element("jugadores");
            if (jugadores == null)
            {
                jugadores = new XElement("jugadores");
                root.Add(jugadores);
            }

            return jugadores;
        }

        private void grabarArchivoXML()
        {
            document.Save(fullName);
        }

        private XElement crearElementoJugadorXML(Jugador objActual)
        {
            XElement elemento = new XElement("jugador",
                                                new XAttribute("Id", objActual.Id),
                                                new XAttribute("Nombre", objActual.Nombre),
                                                new XAttribute("Apellido", objActual.Apellido),
                                                new XAttribute("Dni", objActual.Dni),
                                                new XAttribute("FechaNacimiento", objActual.FechaNacimiento.ToString("dd/MM/yyyy")),
                                                new XAttribute("Localidad",objActual.Localidad),
                                                new XAttribute("Email",objActual.email)
                                );
            return elemento;
        }

        public static PersistentAdapter getInstance()
        {
            if (instance == null)
            {
                instance = new PersistentAdapter();
            }

            return instance;
        }

        public List<Jugador> jugadores()
        {
            var result = from Jugador in obtenerNodoJugadores().Elements()
                         select new
                         {
                             Id = Jugador.Attribute("Id").Value,
                             Nombre = Jugador.Attribute("Nombre").Value,
                             Apellido = Jugador.Attribute("Apellido").Value,
                             DNI = Jugador.Attribute("Dni").Value,
                             FechaNacimiento = Jugador.Attribute("FechaNacimiento").Value,
                             Email = Jugador.Attribute("Email").Value,
                             Localidad = Jugador.Attribute("Localidad").Value,
                         };

            List<Jugador> jugadores = new List<Jugador>();

            foreach (var item in result)
            {
                Jugador j = new Jugador(item.Id, item.Nombre, item.Apellido, item.DNI, item.FechaNacimiento, item.Localidad, item.Email);
                jugadores.Add(j);
            }

            return jugadores;
        }
    }
}
